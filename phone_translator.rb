# Read phone and initiate parsing sequence
class PhoneTranslator
  attr_reader :results

  DICTIONARY_FILE_PATH = 'lib/dictionary.txt'.freeze
  MAP = {
    '2' => '[ABC]',
    '3' => '[DEF]',
    '4' => '[GHI]',
    '5' => '[JKL]',
    '6' => '[MNO]',
    '7' => '[PQRS]',
    '8' => '[TUV]',
    '9' => '[WXYZ]'
  }.freeze

  def initialize(phone)
    @phone = phone
    @results = []
  end

  def parse_file
    first_matches = grep_file(/#{build_regex_combinations.join('|')}/).flatten
    first_matches.each do |matched|
      tails = grep_file(/^#{build_regex[matched.size..@phone.size].join}$/)
      tails.each do |tail|
        @results.push([matched, tail])
      end
    end
    grep_file(/^#{build_regex.join}$/).each { |m| @results.push(m) }
  end

  private

  def build_regex
    @phone.split('').map { |d| MAP[d] }
  end

  def grep_file(pattern)
    File.open(DICTIONARY_FILE_PATH).grep(pattern).map(&:chomp)
  end

  def build_regex_combinations
    (2..7).map { |r| "^#{build_regex[0..r].join}$" }
  end
end

def check_input_data(phone)
  raise ArgumentError, '10 digits should be in phone number' if phone.nil? || phone.size != 10
  raise ArgumentError, 'Number should not include 0 or 1' if phone.include?('0') || phone.include?('1')
end

def translate(phone)
  check_input_data(phone)
  translator = PhoneTranslator.new phone
  translator.parse_file
  translator.results
end

if __FILE__ == $0
  print "#{translate(ARGV[0])}\n"
end
