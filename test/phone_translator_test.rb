require_relative '../phone_translator'
require 'test/unit'

# testing phone translator class
class PhoneTranslatorTest < Test::Unit::TestCase
  def test_map
    assert_equal('[ABC]', PhoneTranslator::MAP['2'])
    assert_equal('[WXYZ]', PhoneTranslator::MAP['9'])
  end

  def test_build_regex
    number = '2222222222'
    assert_equal(
      [
        '[ABC]', '[ABC]', '[ABC]',
        '[ABC]', '[ABC]', '[ABC]',
        '[ABC]', '[ABC]', '[ABC]',
        '[ABC]'
      ], PhoneTranslator.new(number).send(:build_regex)
    )
  end

  def test_dictionary_path
    assert_equal(PhoneTranslator::DICTIONARY_FILE_PATH, 'lib/dictionary.txt')
  end

  def test_grep
    number = '6686787825'
    assert_equal(['ABA', 'BAA', 'CAB'], PhoneTranslator.new(number).send(:grep_file, /^[ABC][ABC][ABC]$/))
  end

  def test_initialization_data
    number = '6686787825'
    assert_equal([], PhoneTranslator.new(number).results)
  end

  def test_first_number_parsing
    phone = '6686787825'
    assert_equal(
      [
        ['MOTOR', 'TRUCK'],
        ['MOTOR', 'USUAL'],
        ['NOUN', 'STRUCK'],
        ['NOUNS', 'TRUCK'],
        ['NOUNS', 'USUAL'],
        ['ONTO', 'STRUCK'],
        'MOTORTRUCK'
      ],
      translate(phone)
    )
  end

  def test_second_number_parsing
    phone = '2282668687'
    assert_equal(
      [
        ['ACT', 'AMOUNTS'],
        ['ACT', 'CONTOUR'],
        ['ACTA', 'MOUNTS'],
        ['BAT', 'AMOUNTS'],
        ['BAT', 'CONTOUR'],
        ['CAT', 'AMOUNTS'],
        ['CAT', 'CONTOUR'],
        'CATAMOUNTS'
      ],
      translate(phone)
    )
  end
end
